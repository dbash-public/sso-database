const { Pool, Client } = require('pg');
// Postgres does not support parameterized queries in CREATE user / ALTER user
const escape = require('pg-escape');
const env = process.env;
const pool = new Pool({
    user: env.PG_USERNAME,
    host: env.PG_HOSTNAME,
    database: env.PG_DATABASE,
    password: env.PG_PASSWORD,
    port: env.PG_PORT,
})

export async function upsertUser(username: string, password: string) {
    // The below query uses a prepared statement, the rest in this chain cannot unfortunately
    // https://www.postgresql.org/message-id/925094a20608222109s438a5b41g2886f41e9ddf7417@mail.gmail.com
    const user = await getUser(username);
    username = escape(username);
    password = escape(password);
    if (!user) {
        return await createUser(username, password);
    }

    return updateUser(username, password);
}

async function getUser(username: string) {
    const query = `SELECT usename, usecreatedb, usesuper FROM pg_catalog.pg_user WHERE usename = $1 LIMIT 1;`;
    const result =  await pool.query(query, [username]);
    if (result.rows.length === 0) {
        return false;
    }
    return result.rows[0];
}

async function createUser(username: string, password: string) {
    const query = `CREATE USER ${username}`;
    const result =  await pool.query(query);
    await updateUser(username, password);
    return result;
}

async function updateUser(username: string, password: string) {
    console.log('update user');
    const query = `alter user ${username} with encrypted password '${password}';`;
    const alterUser = await pool.query(query);
    const permissions = `GRANT ${getPermissions()} ON DATABASE ${env.PG_DATABASE} TO ${username};`
    const alterPermissions =  await pool.query(permissions);

    return [alterUser, alterPermissions];
}

function getPermissions() {
    if (!env.PG_DEFAULT_PERMISSIONS) {
        return 'SELECT';
    }
    
    return env.PG_DEFAULT_PERMISSIONS;
}