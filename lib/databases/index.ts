import {Application} from "express";
import setupPassport from "./Google";
import {upsertUser} from "./postgres/User";
import {randomBytes} from "crypto";
export async function upsertDb(req: Request, database: object) {
    const session = req.session;
    const username = session.passport.user.username;
    let password = randomBytes(16).toString('hex');

    if (database.type === 'pg') {
        await upsertUser(username, password);
    }

    return {
        username, password
    }

}