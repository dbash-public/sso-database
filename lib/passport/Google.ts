import {Application} from 'express';
import passport from 'passport';
import {randomBytes} from "crypto";
import {upsertUser} from "../databases/postgres/User";
const GoogleStrategy = require('passport-google-oauth20').Strategy;

export default function setupPassport(app: Application) {
    passport.use(new GoogleStrategy({
            clientID: process.env.G_OAUTH_API_KEY,
            clientSecret: process.env.G_OAUTH_SECRET,
            callbackURL: `http://localhost:${process.env.PORT}/oauth2/redirect/accounts.google.com`,
            scope: [ 'profile', 'email' ],
        },
        function(accessToken: string, refreshToken: string, profile, cb) {
            console.log(profile);
            let username = getUsername(profile);
            cb(null, {username: username});
        }
    ));

    passport.serializeUser(function(user, cb) {
        process.nextTick(function() {
            cb(null, { username: user.username});
        });
    });

    passport.deserializeUser(function(user, cb) {
        process.nextTick(function() {
            return cb(null, {username: "from session"});
        });
    });

    app.get('/login/google',
        passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile'] }));
    app.get('/oauth2/redirect/accounts.google.com',
        passport.authenticate('google', { failureRedirect: '/login', failureMessage: true }),
        async function(req, res) {
            res.redirect('http://localhost:3000');
            // req.session.active = true;
            // let user = req.user;
            // let password = randomBytes(10).toString('hex');
            //
            // let created = await upsertUser(user.username, password);
            // res.send(`Login with username: ${user.username} & password: ${password}`)
        });
}

function getUsername(profile, req) {
    let username = '';
    if (req && req.session.username) {
        return req.session.username;
    }
    if (profile.displayName) {
        username = profile.displayName.toLowerCase().replace(' ', '_');
    } else if (profile.familyName) {
        username = profile.name.join('_').toLowerCase();
    }
    if (username === '') {
        throw Error('Unable to parse user profile');
    }

    return username;
}


