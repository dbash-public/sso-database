// import {Application} from 'express';
// import passport from 'passport';
// import {randomBytes} from "crypto";
// import {upsertUser} from "./lib/postgres/User";
// const GoogleStrategy = require('passport-google-oauth20').Strategy;
// import
// enum authMethod {
//     azure = 'azure',
//     google = 'google',
//     ldap = 'ldap',
//     saml2 = 'saml2',
//     local = 'local'
// }
//
//
//
// export default function setupPassport(app: Application) {
//     switch (process.env.AUTH_METHOD) {
//         case 'google': {
//             startupGoogleAuth(app);
//         }
//     }
// }
//
// function getUsername(profile, method: authMethod) {
//     let username = '';
//     if (method === 'google') {
//         if (profile.displayName) {
//             username = profile.displayName.toLowerCase().replace(' ', '_');
//         }
//     }
//
//     if (username === '') {
//         throw Error('Unable to parse user profile');
//     }
//
//     return username;
// }
//
//
// function startupGoogleAuth(app: Application) {
//     passport.use(new GoogleStrategy({
//             clientID: process.env.G_OAUTH_API_KEY,
//             clientSecret: process.env.G_OAUTH_SECRET,
//             callbackURL: "http://localhost:3000/oauth2/redirect/accounts.google.com",
//             scope: [ 'profile' ]
//         },
//         function(accessToken: string, refreshToken: string, profile, cb) {
//             console.log(profile);
//             let username = getUsername(profile, 'google');
//             cb(null, {username: username});
//         }
//     ));
//
//     passport.serializeUser(function(user, cb) {
//         process.nextTick(function() {
//             cb(null, user);
//         });
//     });
//
//     passport.deserializeUser(function(user, cb) {
//         process.nextTick(function() {
//             return cb(null, user);
//         });
//     });
//
//     app.get('/login/google',
//         passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile'] }));
//     app.get('/oauth2/redirect/accounts.google.com',
//         passport.authenticate('google', { failureRedirect: '/login', failureMessage: true }),
//         async function(req, res) {
//             let user = req.user;
//             let password = randomBytes(10).toString('hex');
//
//             let created = await upsertUser(user.username, password);
//             console.log(req.query);
//             res.send(`Login with username: ${user.username} & password: ${password}`)
//         });
// }