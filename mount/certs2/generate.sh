openssl genrsa -des3 -out server.key 2048
openssl rsa -in server.key -out server.key
chmod og-rwx server.key
openssl req -new -x509 -days 3650 -subj '/C=US/ST=FL/L=Orlando/O=dbash/CN=dbash' -key server.key -out server.crt
cp server.crt root.crt
openssl genrsa -des3 -out postgresql.key 2048
openssl rsa -in postgresql.key -out postgresql.key
openssl req -new -subj '/C=US/ST=FL/L=Orlando/O=dbash/CN=postgres' -key  postgresql.key -out postgresql.csr
