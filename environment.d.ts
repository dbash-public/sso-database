declare global {
    namespace NodeJS {
        interface ProcessEnv {
            USERNAME_FORMAT: string;
            DB_POSTGRES_PROXY_PORT: number;
            PG_USERNAME: string;
            PG_HOSTNAME: string;
            PG_PASSWORD: string;
            PG_PORT: number;
            PG_DATABASE: string;
            PG_DEFAULT_PERMISSIONS: string;
            AUTH_METHOD: 'google' | 'azure' | 'ldap' | 'saml2' | 'local' | 'demo';
            G_OAUTH_API_KEY: string;
            G_OAUTH_SECRET: string;
            WEB_HOST_NAME: string;
            WEB_PORT: number;
        }
    }
}