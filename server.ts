import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import { randomBytes } from 'crypto';
import { upsertDb } from './lib/databases/index';
import cors from 'cors';
import { writeFileSync } from 'fs';
import * as bodyParser from 'body-parser';
const app = express();
const port = process.env.PORT || 3001;
app.use(cors({
    credentials: true,
    "origin": "http://localhost:3000",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
}));

import server from './proxy';
app.use(express.static("public"));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

server.listen(process.env.DB_POSTGRES_PROXY_PORT, () => {
    console.log('Connect postgres to: localhost:' + process.env.DB_POSTGRES_PROXY_PORT);
});
import setup from './lib/passport';
setup(app);

if (process.env.AUTH_METHOD === 'demo') {
    app.get('/login', async (req, res) => {
        let {query} = req;
        let password = randomBytes(10).toString('hex');
        if (!query.username) {
            return  res.send(`Please specify a username`);
        }
        let user = await upsertDb(query.username, password);
        res.send(`Login with username: ${req.query.username} & password: ${password}`)
    })
}

app.post('/databases/:id', async (req, res) => {
    let databases = require('./databases.json');
    let match = databases.find(d => d.id === req.params.id);
    if (!match) {
        return res.json({
            notFound: true
        }).status(404);
    }
    let credentials = await upsertDb(req, match);
    res.json(credentials);
});


app.get('/databases', (req, res) => {
    let databases = require('./databases.json');
    const filtered  = databases.map((data: object) => {
        return {
            id: data.id,
            hostname: data.hostname,
            port: data.port,
            type: data.type,
            database: data.database,
            name: data.name,
            id: data.id
        }
    });
    res.json(filtered);
});

let providerMap = {
    google: 'http://localhost:3001/login/google'
}
app.get('/provider', (req, res) => {
    res.json({
        provider: process.env.AUTH_METHOD,
        location: providerMap[process.env.AUTH_METHOD]
    });
});


app.get('/session', function(req, res, next) {
    res.json(req.session);
});

app.listen(port, () => {
    console.log(`db-proxy web server running at http://localhost:${port}`)
})


function setupDatabases() {
    try {
        let databases = require('./databases.json');
        let changes = false;
        for (let database of databases) {
            if (!database.id) {
                changes = true;
                database.id = randomBytes( 16).toString('hex');
            }
        }
        if (changes) {
            writeFileSync('./databases.json', JSON.stringify(databases));
        }
    } catch (err) {
        console.error('No databases.json found');
    }

}
setupDatabases();