const glob = require("glob");
const fs = require('fs');
const mime = require('mime');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const cloudfront = new AWS.CloudFront();
const getDirectories = function (src, callback) {
  glob(src + '/**/*', callback);
};
getDirectories('./build', async function (err, files) {
  let promises = [];
  for (let file of files) {
    console.log(file);
    const stat = fs.statSync(file);
    if (stat.isDirectory() === true) {
      continue;
    }
    const mimeType = mime.getType(file);
    const params = {
      Bucket: 'remove-pii',
      Key: file.substr(8), // File name you want to save as in S3
      Body: fs.readFileSync(file),
      ContentType: mimeType
    };

    promises.push(uploadPromise(params));
  }
  if (err) {
    console.log('Error', err);
  } else {
    console.log(files);
  }

  await Promise.all(promises);
  try {
    invalidateCache();
  } catch (err) {
    console.log(err);
  }
});


async function uploadPromise(params) {
  return new Promise((resolve, reject) => {
    // Uploading files to the bucket
    s3.upload(params, function (err, data) {
      if (err) {
        return reject(err);
      }
      console.log(`File uploaded successfully. ${data.Location}`);
      return resolve(data);
    });
  })
}

async function invalidateCache() {
  const items = [
    '/*'
  ];
  let params = {
    DistributionId: 'E2KB6UCEFAJOK8', /* required */
    InvalidationBatch: { /* required */
      CallerReference: 'deploy ' + new Date().getTime(), /* required */
      Paths: { /* required */
        Quantity: items.length.toString(), /* required */
        Items: items
      }
    }
  };
  return cloudfront.createInvalidation(params, function (err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else console.log(data);           // successful response
  });
}