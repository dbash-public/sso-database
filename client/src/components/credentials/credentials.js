import { Badge, Button } from 'components/lib';

export function Credentials(props){

    return(
      <div style={{textAlign: "center"}}>
          <Button
              iconButton = { true }
              iconColor = 'white'
              color = 'green'
              icon = 'copy'
              size = { 32 }
              text ={ `Copy ${props.database.hostname}:${props.database.port}` }
              action ={ () => navigator.clipboard.writeText(`${props.database.hostname}:${props.database.port}`) }
          />
          <br />
          <br />
          <Button
              iconButton = { true }
              iconColor = 'white'
              color = 'blue'
              icon = 'copy'
              size = { 32 }
              text = 'Copy username'
              action ={ () => navigator.clipboard.writeText(props.credentials.username) }
          />
          &nbsp;
          <Button
              iconButton = { true }
              iconColor = 'white'
              color = 'red'
              icon = 'copy'
              size = { 32 }
              text = 'Copy password'
              action ={ () => navigator.clipboard.writeText(props.credentials.password) }
          />


      </div>
  );

}
