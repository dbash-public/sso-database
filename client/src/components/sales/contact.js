import React, { Fragment, useState } from 'react';
import { Form, Message} from 'components/lib';

export function SalesContact(props){

  const [sent, setSent] = useState(false);
  let options = [
    {label:'1-100', value: '100'},
    {label: '101-250', value: '250'},
    {label: '251-500', value: '500'},
    {label: '501-1000', value: '1000'},
    {label: '1000+', value: '+++++'},
  ];

  return (
    <Fragment>

      { sent ? 
        <Message  
          title='Message Sent'
          type='success'
          text={ `Thank you for your message, we'll be in touch as soon as possible.` }
        />  :

        <Form 
          data={{ 
            name: {
              label: 'Your name',
              type: 'text',
              required: true,
              errorMessage: 'Please enter your name'
            },
            email: {
              label: 'Your email',
              type: 'email',
              required: true,
              errorMessage: 'Please enter your email address'
            },
            companyName: {
              label: 'Company Name',
              type: 'text',
              required: true,
              errorMessage: 'Please enter your industry'
            },
            employees: {
              label: 'Number of users',
              type: 'select',
              required: true,
              value: options[0].value,
              options: options,
              errorMessage: 'Please enter a valid selector'
            },
            message: {
              label: 'Additional Comments',
              type: 'textarea',
              required: false,
              errorMessage: 'Please enter a message'
            } 
          }}
          method='POST'
          url='/api/utility/mail/raw'
          callback={ e => setSent(true) }
          buttonText='Send Message'
         />
       }
     </Fragment>
  );
}
