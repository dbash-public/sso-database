import React, { Fragment, useState } from 'react';
import { Loader, useAPI, Form } from 'components/lib';
import { useHistory } from "react-router-dom";

export function Reports(props){

  const [state, setState] = useState({});
  const reportConfig = useAPI('/api/reports');
  const testing = useHistory();

  if(props.computedMatch.params.type) {
    console.log(props.computedMatch.params);
  }
  if (!reportConfig || !reportConfig.data)
    return false;

  let userMenu = [];
  let dayMenu = [];
  let typeMenu = [];
  for (let user of reportConfig.data.users) {
    userMenu.push({ value: user.id, label: user.email});
  }

  for (let day of reportConfig.data.days) {
    dayMenu.push({ value: day, label: day});
  }

  for (let type of reportConfig.data.types) {
    typeMenu.push({ value: type, label: type});
  }

  function generateReport(params) {
    testing.push(params.data.config.url);
  }

  return(
    <Fragment>

      { reportConfig?.data &&

      <Form
        buttonText='Run'
        url= {'/api/reports/run'}
        method='POST'
        data={{
          dateRange: {
            label: 'Generate for last (x) days',
            type: 'select',
            options: dayMenu,
            default: dayMenu[0]
          },
          user: {
            label: 'User',
            type: 'select',
            options: userMenu,
            default: userMenu[0]
          },
          type: {
            label: 'Type of Action',
            type: 'select',
            options: typeMenu,
            default: typeMenu[0]
          }
        }}
        callback={generateReport}
      />
      }

    </Fragment>

  );
}
