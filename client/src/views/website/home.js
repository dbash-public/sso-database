import React, { Fragment, useState, useEffect } from 'react';
import {Row, Hero, Credentials, Databases, Provider, APIState} from 'components/lib';

export function Home(props) {

    const [progress, setProgress] = useState('awaitingLogin');
    const [session, setSession] = useState(null);
    const [database, setDatabase] = useState(null);
    const [credentials, setCredentials] = useState(null);

    useEffect(() => {
        APIState(setSession,  '/session');
    }, []);

    useEffect(() => {
        if (credentials?.data?.username) {
            setProgress('showCredentials');
        } else if (session?.data?.passport) {
            setProgress('selectDatabase');
        }
    }, [credentials, session, database]);

    let content;
    switch (progress) {
        case 'awaitingLogin':
            content = <Row title='SSO Login' color='white'><Provider /></Row>;
            break;
        case 'selectDatabase':
            content = <Row title='Select a Database' color='white'><Databases setCredentials={setCredentials} setDatabase={setDatabase} /></Row>;
            break;
        case 'showCredentials':
            content = <Row title='Your Credentials' color='white'><Credentials credentials={credentials.data} database={database} /></Row>;
            break;
        default:
            return null;
    }

    return(
    <Fragment>
        <Hero
            title="SSO into your DB"
            tagline="Derp Herp."
            />
        {content}
    </Fragment>
  );
}
