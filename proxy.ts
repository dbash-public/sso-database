import * as net from 'net';
let remotehost = 'localhost';
let remoteport = 5400;
let lastPacket;
let server = net.createServer(function (localsocket) {
    let remotesocket = new net.Socket();

    remotesocket.connect(remoteport, remotehost);

    localsocket.on('connect', function (data) {
        console.log(">>> connection #%d from %s:%d",
            server.connections,
            localsocket.remoteAddress,
            localsocket.remotePort
        );
    });

    localsocket.on('data', function (data) {
        console.log("client->server - writing data to remote",
            localsocket.remoteAddress,
            localsocket.remotePort
        );
        console.log(data);
        console.log(data.toString());

        let flushed = remotesocket.write(data);
        if (!flushed && !localsocket.destroyed) {
            console.log("  remote not flushed; pausing local");
            localsocket.pause();
        }
    });

    remotesocket.on('data', function(data) {
        console.log("server->client - writing data to local",
            localsocket.remoteAddress,
            localsocket.remotePort
        );
        console.log(data);
        lastPacket = data;
        console.log(data.toString());
        console.log(data.toString('hex'));

        let flushed = localsocket.write(data);
        if (!flushed && !remotesocket.destroyed) {
            console.log("  local not flushed; pausing remote");
            remotesocket.pause();
        }
    });

    localsocket.on('drain', function() {
        console.log("%s:%d - resuming remote",
            localsocket.remoteAddress,
            localsocket.remotePort
        );
        remotesocket.resume();
    });

    remotesocket.on('drain', function() {
        console.log("%s:%d - resuming local",
            localsocket.remoteAddress,
            localsocket.remotePort
        );
        localsocket.resume();
    });

    localsocket.on('close', function(had_error) {
        console.log("%s:%d - closing remote",
            localsocket.remoteAddress,
            localsocket.remotePort
        );
        remotesocket.end();
    });

    remotesocket.on('close', function(had_error) {
        console.log("%s:%d - closing local",
            localsocket.remoteAddress,
            localsocket.remotePort
        );
        localsocket.end();
    });

});

export default server;
